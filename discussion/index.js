console.log("Hello World");

console.log("Drink HTML");
console.log("Eat Javascript");
console.log("Inhale CSS");
console.log("Bake Bootstrap");


let task = ["Drink HTML", "Eat Javascript", "Inhale CSS", "Bake Bootstrap"]
console.log(task)

// to display the first element
console.log(task[0]);

// to display the total of elements in an array

let indexOfLastElement = task.length-1
console.log(task[indexOfLastElement]);
	
let indexLastElement = task.length-3
console.log(task[indexLastElement]);

// Array Manipulation

// setting the array
let numbers = ["one","two","three","four",]
console.log(numbers)

// adding an elements e.g when you forgot an element in an array
// PUSH Method - this is useful if we're going to insert 2-3 elements, but for more use FUNCTION
// limitation: you can only add elements AT THE END of an array
	/*
	Syntax: arrayName.push(element)
	*/
numbers.push("five")
console.log(numbers);

function pushMethod(element){
	numbers.push(element)
}
pushMethod("six")
pushMethod("seven")
pushMethod("eight")
console.log(numbers)

// adding an element to be removed
// POP METHOD
pushMethod("CrushAkoNgCrushKo")
console.log(numbers)

numbers.pop()
console.log(numbers)

function popMethod(element){
	numbers.pop(element)
}
popMethod()
console.log(numbers)
// --------------------------------------------------

// adding an element at the beginning of an array
// Unshift Method
 // adding an element at the start of an array
numbers.unshift("zero")
console.log(numbers)

function unshiftMethod(element){
	numbers.unshift(element)
}
unshiftMethod("jm pogi")
console.log(numbers)
// --------------------------------
// removing an element at the beginning of an array
// SHIFT Method

numbers.shift()
console.log(numbers)


function shiftMethod(){
	numbers.shift()
}
shiftMethod()
console.log(numbers)

// -----------------------------
let numbs = [15, 10,31, 24, 30]

// SORT METHOD
	// arranges the order of the elements in the array
	// ASCENDING ORDER
	numbs.sort(
		function(a, b){
			return a-b
		}
	   )
	console.log(numbs)

	// DESCENDING ORDER
	numbs.sort(
		function(a, b){
			return b-a
		}
	   )
	console.log(numbs)

// REVERSE METHOD
// reverses the order of the elements of the last displayed array
	numbs.reverse()
	console.log(numbs)

	numbs.reverse()
	console.log(numbs)

 //SPLICE METHOD
 	 //It directly manipulates the array
 	 //It returns the omitted elements 

let nums = numbs.splice(3, 1, 11, 9, 1)
console.log(numbs)
console.log(nums)

// SLICE METHOD 

let slicedNumbs = numbs.slice(1, 6)
console.log(slicedNumbs)
console.log(numbs)

// --------------------------------
// CONCATENATE
	// merge two or more arrays
	console.log(numbers)
	console.log(numbs)
	let animals = ["(snake", "chimp", "dog", "cat"]
	console.log(animals)	
/* 
parameters - determines the arrays to be merged with the "numbers" array
*/ 
	let newConcat = numbers.concat(numbs, animals)
	console.log(newConcat)

	let newConcat1 = animals.concat(numbs, numbers)
	console.log(newConcat1)
// the concatenated array can be merged again to another array
	let animals2 = ["birds", "dinosaur"]
	let concat2 = newConcat.concat(animals2)
	console.log(concat2)

// JOIN METHOD
	//merges the elements inside the array
	// no parameter - transform the data type of the array into strings, separated by comma
let meal = ["rice", "lechon kawali", "Coke"]
console.log(meal)

let newJoin = meal.join()
console.log(newJoin)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)

// Accessors

// indexOf() arrayName.indexOf(element)
 // determines the index (order count) of the speficied element/parameter

let countries = ["PKT", "US", "PH", "NZ","UK","AU","SG","JPN","CA", "SK", "PH"]
console.log(countries.indexOf("PH"))
// if the element is non-existing, it will return -1
console.log(countries.indexOf("RU"))
console.log(countries.indexOf(""))

// lastIndexOf
	// starts the searching of the element from the end of the array.
console.log(countries.lastIndexOf("PH"))

//Reassignment of the elements and index
countries[0] = "NK"
console.log(countries) 

// Iterators
 // forEach (cb())

 let days = ["mon", "tue", "wed", "thur", "fri", "sat", "sun"]
 days.forEach(
 	function(element){
 		console.log(element)
 	 }
 	)

// MAP METHOD (cb())
let mapDays = days.map(
	function(day){
		return '${day} is the week'
	 }
	)
	console.log(mapDays)
	console.log(days)

// ----------------------

// Mini-activity

// forEach (cb())

 let days2 = [];
 console.log(days2)

 days.forEach(function(day){
 		days2.push(day)
 	 }
 	)
console.log(days2)

// every method checks if every element meets the condition set
let numberA = [1, 2, 3, 4, 5]

let allValid = numberA.every(e => e < 3)

console.log(allValid)

// some method parang OR

let someValid = numberA.some( e => e<2)

console.log(someValid)

// filter method
let filtered = numberA.filter(e => e < 3)
console.log(filtered)

let fil = []
numberA.forEach(e => e<3 ? fil.push(e) : false)
console.log(fil)


let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor', 'All']
let filteredProduct = products.filter(e => e.toLowerCase().includes('mon'))
console.log(filteredProduct)

let x = []
let y =[]
c = ''


function nextChar(c, i) {
    return String.fromCharCode(c.charCodeAt(0) + i);
}
nextChar('a');

for(let i = 0; i <= 8; i++) {
	// x.push(nextChar('a', i))
	for(let j = 1; j <= 8; j++) {
		x.push(nextChar('a', i)+j)
	}
	y.push(x)	
}

// console.log(x)
console.log(y)

// function nextChar(c) {
//     return String.fromCharCode(c.charCodeAt(0) + 1);
// }
// nextChar('a');
